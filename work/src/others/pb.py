#from functools import reduce 

def calling(val, list1):
    """ Evaluates the polynom """
    lista = list1 #lista original.[3,2,1]
    n1 = list(range(len(lista))) 
    n1.reverse() # lista para elevar [2,1,0] son mis exponentes.
    n2 = [val for x in n1]
    n3 = map(lambda x : pow(x), zip(n2,n1)) #despues de un zip, [(3,2),(2,1),(1,0)]
    c = list(n3) # c es mi lista de x elevados.
    n4 = map(lambda x: calc(x), zip(lista,c))
    n = list(n4)
    return n

def pow(x):
    a , b = x
    return a**b
    
def calc(x):
    a, b = x
    return a * b

