from functools import reduce

# edlobo98@gmail.com

# list = [6,0,-2,5,-3]
# number (newton-raphson) = 2

def ruffini(lis, number):
    #nlist = reduce(mult,lis[1:],lis[0])
    #print(nlist)
    #p = lis[1:] # esto recorta la lista en un elemento.
    lista  = lis
    numero = number
    aux=lis[0]
    nlista= lis[1:]
    res = [aux]
    #resp = reduce(lambda x,y: x+[(numero*aux)],nlista,res) Es igual a lo de abajo
    resp = reduce(lambda x,y: x+[(numero*res[len(res)-1])],nlista,res)
    resp.pop()
    return resp
    

def sum(x,y):
    return x+y
def mult(x, y):
    return x if x > y else y