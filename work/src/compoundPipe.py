"""
Demo irrelevante sobre interes compuesto solo para ejercicio de FP
@author loriacrlos@gmail.com
@since II-2018
"""
from functools import reduce, partial
import timeit
from util import mapAc

def getPayment(monthly_rate, principal, term_in_months):
    # month=1
    # while month < term_in_months+1:
    #     # yield principal * (monthly_rate)**(month)
    #     principal *=(1 + monthly_rate)
    #     yield principal
    #     month+=1
    yield from mapAc(lambda x,ac: ac*(1+monthly_rate),[None] * term_in_months,principal)

def total_amount(payments):
    total = reduce((lambda x,y: x+y),payments)
    return total

def pipe(init, *iterables):
    return reduce(lambda a, n: n(a), iterables, init)  

def print_payments(margin, separation, payments):   
    for i,x in enumerate(payments):
        print(f"{margin}{(i+1):^5d}{separation}{x:>8,.2f}")
    return x
    
    
def print_table(conditions):
    margin = " " * 3
    separation = " " * 3

    principal = conditions['principal']
    monthly_rate = conditions['yearly_rate'] / 1200
    term_in_months = conditions['term_in_years'] * 12
    partial_printPayments = partial(print_payments, margin, separation)
    
    print(">> Conditions: principal=${principal:0,.2f}; rate={yearly_rate:0.2f}annually%;  term={term_in_years:0d}years <<".format(**conditions))
    header = f"{margin}Month{separation}Principal"
    underline = "-" * ( len(header) + len(margin) )
    print(underline)
    print(header)
    print(underline)   
    
    result = pipe(
                getPayment(monthly_rate, principal, term_in_months), 
                partial_printPayments
    )
    print(underline)
    footer = f"{margin}{term_in_months:^5d}{separation}{result-principal:,>0,.2f}"
    print(footer)
    
   
    
def test():
    print("*** Simple Compound Interest ***")
    conditions = {'principal' : 10_000, 'yearly_rate' : 21, 'term_in_years' :2}
    print_table(conditions)
    

if __name__ == "__main__":
    test()
    
