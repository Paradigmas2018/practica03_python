"""
Demo irrelevante sobre interes compuesto solo para ejercicio de FP
@author loriacrlos@gmail.com
@since II-2018
"""
from functools import reduce
from util import mapAc

def compound_interest(conditions):
    principal, yearly_rate, term_in_years = (conditions['principal'], 
                                             conditions['yearly_rate'], 
                                             conditions['term_in_years']) 
    term_in_months = term_in_years * 12
    payments = [None] * term_in_months
    monthly_rate = yearly_rate / 1200
    # payments = [ principal * ( 1 + monthly_rate )**(i) for i in range(1,1+term_in_months) ]
    # payments = list(map(lambda i: principal * ( 1 + monthly_rate )**(i), range(1,1+term_in_months)))
    payments =  list(mapAc(lambda x,ac: ac*(1+monthly_rate),payments,principal))
    return payments

def total_amount(payments):
    total = list(reduce((lambda x,y: x+y),payments))
    return total
    
def growth(payments, conditions):
    return payments[-1] - conditions['principal']
    
def print_table(conditions):
    #
    payments = compound_interest(conditions)
    total_growth = growth(payments, conditions)
    #
    margin = " " * 3
    separation = " " * 3
    print(">> Conditions: principal=${principal:0,.2f}; rate={yearly_rate:0.2f}annually%;  term={term_in_years:0d}years <<".format(**conditions))
    header = f"{margin}Month{separation}Principal"
    underline = "-" * ( len(header) + len(margin) )
    print(underline)
    print(header)
    print(underline)
    n=len(payments)
    [ print(f"{margin}{(i+1):^5d}{separation}{x:>8,.2f}") for i,x in enumerate(payments) ]       
    print(underline)
    footer = f"{margin}{n:^5d}{separation}{total_growth:,>0,.2f}"
    print(footer)
   
    
def test():
    print("*** Simple Compound Interest ***")
    conditions = {'principal' : 10_000, 'yearly_rate' : 21, 'term_in_years' :2}
    print_table(conditions)

if __name__ == "__main__":
    test()