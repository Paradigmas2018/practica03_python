def mapAc(func, iterable, ac=None):
    it = iter(iterable)
    if ac==None:
        ac = next(it)
        yield ac    
    for x in it:        
        ac=func(x,ac)
        yield ac