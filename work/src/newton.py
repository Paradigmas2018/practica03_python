from functools import reduce
from todo import *

"""
Demo irrelevante sobre newthon-raphson solo para ejercicio de FP
Se debe implementar todo lo que dice 'todo'
@author loriacarlos@gmail.com
@since II-2018
"""


class Poly(list):
    """
        Polynom as coeffcients list
    """

    def __init__(self, *args, var="x"):
        """ Creates polynom """
        super(Poly, self).__init__(args)
        self.var = var
        self.epsilon = 1e-5
        self.max = 20
        if isinstance(args,tuple) == True:
            self.mlist = args[0]
        else:
            self.mlist = list(args)
        # todo(self.__init__, "Must be inmutable",
        # "args must a list of numbers")

    def __str__(self):
        # todo(self.__str__)
        lista = self.mlist
        ultimo = lista[-1]
        lista2 = lista[0:-1]
        contador = len(lista2)
        res = "";
        for x in lista2:
            res += f"{x}x**{contador}+"
            contador -= 1
        res += f"{ultimo}"
        return f"Poly: {res}"

    def __repr__(self):
        # todo(self.__repr__)
        return f"Poly: {self.mlist}, Var: {self.var}, Epsilon: {self.epsilon}, Max: {self.max}"

    @property
    def dx(self):
        """ Derivates the polynome """
        mylist = self.mlist[0:-1]
        n1 = list(range(len(mylist)))
        n2 = list(range(len(mylist)))
        n1.reverse()
        a = list(zip(mylist, n1))
        form = list(map(lambda x: self.eval(x), zip(mylist, n1)))
        #d = dict(zip(n2,form))
        p = Poly(form) #aqui lo enviamos como lista.... es lo que falla.
        self.mydx = p
        return self.mydx

    def __call__(self, val):
        """ Evaluates the polynom """
        #self.value = val  # aqui defino con lo que se evaluara la expresion.
        lista = self.mlist  # lista original recibida.
        n1 = list(range(len(lista)))
        n1.reverse()  # lista para elevar [2,1,0] son mis exponentes.
        n2 = [val for x in n1]
        n3 = list(map(lambda a: self.pow(a), zip(n2, n1)))
        n = reduce(lambda a, b: a + b, list(map(lambda a: self.mul(a), zip(lista, n3))))
        # Hay que hacer un reduce para que dé un número como respuesta
        # a = reduce(lambda a , b : a + b,n)
        return n

    def resta(self, x):
        a, b = x
        return a - b

    def pow(self, x):
        a, b = x
        return a ** b

    def eval(self, x):
        a, b = x
        return a * (b + 1)

    def mul(self, x):
        a, b = x
        return a * b

    @property
    def degree(self):
        """ Polynom´s degree. Must be aproperty """
        return (len(self) - 1)

    def newt_raph(self, x):
        self.value = x
        # Como cambié el call con un reduce, ya no es necesario hacerlo aquí
        # n1 = reduce(lambda a , b : a + b , self.__call__(x))
        n1 = self.__call__(self, x)
        # Hay que hacer esto mismo de arriba, pero evaluando para la derivada
        # n2 = reduce(lambda a , b : a + b , self.dx.__call__(x))
        n2 = self.dx.__call__(self, x)
        print(n2)
        return (x - (n1 / n2))

    def solve(self, r0=None):
        """ Solves by Newton-raphson starting in r0 """
        if not r0:
            r0 = self[
                     -1] / self.degree()  # takes the last number of the list, and divides into the degree of the polynom
        old = self.newt_raph(r0)
        yield (old)
        new = self.newt_raph(old)
        yield (new)
        while old - new > self.epsilon:
            old = new
            new = self.newt_raph(old)
            yield (new)

    def __floordiv__(self, alpha):
        """ Performs synthetic division by x - alpha """
        todo(self.__floordiv__,
             "alpha must be a number",
             "Returns a new poly"
             )
        return self

    def roots(self):
        """ Finds all root by using solve and // repeatedly """
        #todo(self.roots, "Must be a generator")

        return iter([self.solve()])


if __name__ == "__main__":
    #p = Poly(1, -7, 7, 15)
    p = Poly([16, 136, -40, -784, 384, 0])
    print(p)
    print(p(0.5))
    print(p.dx)
    #print(p.solve())
    print(p // 5)
    # print(list(p.roots()))