from util import mapAc

#rnf -> Ruffini no Funcional
def rnf(lista, numero):
    copy = lista
    number = numero
    aux= lista[0]
    lista2 = copy[1:]
    res=[aux]
    #print(f"lista: {copy} , primero de la lista: {aux} , lista sin el primero: {lista2}")
    # [ aux=(aux*number)+x; aux for x in lista2]
    for x in lista2:
        aux= (aux*number)+x
        res.append(aux)
    res.pop()
    return res
    
def ruf(lista, numero):
    # ac = lista[0]
    # for x in lista[1:]:
    #     yield ac
    #     ac = ac * numero + x
    return list(mapAc(lambda x,ac: numero*ac+x, lista))[0:-1]

